#include <iostream>
#include <random>
#include <memory>
#include <queue>
#include <string>
using namespace std;

//if don't support the c++14 make_unique, define
#if _MSC_VER < 1900
template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&& ... args) {
	return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
#endif //  _MSC_VER < 1900

//class player ,save HP and ATK
class Player
{
public:
	Player(int hp, int atk) :HP(hp), ATK(atk) {};
	void CutHP(int atk)
	{
		//if the hp is smaller than 0,set it 0
		HP -= atk;
		if (HP < 0)
			HP = 0;
	}
	int& GetHP() { return HP; }
	int& GetATK() { return ATK; }
private:
	int HP;
	int ATK;
};
//the Game MushiKing class
class MushiKing
{
private:
	//game state
	enum StateFlag
	{
		NoInput,    // at the begin of game
		WaitInput,  // wait players' input
		UWIn,       // the state of user is win
		UDeuce,     // the state of same input
		ULose,      // the state of user is lose
		OverWin,    // the result state:user win
		OverLose    // the result state:user is lose
	};

public:
	MushiKing()
		:m_user(nullptr),
		m_computer(nullptr),
		m_gameState(NoInput)
	{}

	~MushiKing()
	{}

	//initialize the data of user and computer
	void Initialize(int hpA, int atkA, int hpB, int atkB)
	{
		m_user = make_unique<Player>(hpA, atkA);
		m_computer = make_unique<Player>(hpB, atkB);
	}
	//process input
	void Input(istream& in, queue<int>& computerInput)
	{
		if (m_gameState == NoInput)
		{
			return;
		}
		else if (m_gameState == WaitInput)
		{

			string instr;
			getline(in, instr);

			if (instr == "g" || instr == "c" || instr == "p")
			{
				ChangeStateFromInput(instr, computerInput);
			}
		}
	}
	//update players' HP
	bool Update()
	{

		if (m_gameState == UWIn)
		{
			m_computer->CutHP(m_user->GetATK());
			if (m_computer->GetHP() <= 0)
			{
				m_gameState = OverWin;
				return true;
			}
		}
		else if (m_gameState == ULose)
		{
			m_user->CutHP(m_computer->GetATK());
			if (m_user->GetHP() <= 0)
			{
				m_gameState = OverLose;
				return true;
			}
		}
		return false;

	}
	//process text show
	void ShowText(ostream& out)
	{
		if (m_gameState == NoInput)
		{
			ShowInfoText(out);
			ShowInputText(out);
			m_gameState = WaitInput;
		}
		else if (m_gameState == WaitInput)
		{
			ShowInputText(out);
		}
		else if (m_gameState == UWIn || m_gameState == UDeuce || m_gameState == ULose)
		{
			ShowBattleText(out);
			ShowInfoText(out);
			ShowInputText(out);
			m_gameState = WaitInput;
		}
		else if (m_gameState == OverWin)
		{
			ShowBattleText(out);
			ShowInfoText(out);
			out << "you win !!!" << endl;
		}
		else if (m_gameState == OverLose)
		{
			ShowBattleText(out);
			ShowInfoText(out);
			out << "you lose..." << endl;
		}

	}
	//shutdown game and release resource
	void Shutdown()
	{
		m_user.reset();
		m_computer.reset();
	}

private:
	ostream& ShowBattleText(ostream& out)
	{
		string str = m_userInputStr + " vs " + m_computerInputStr;
		out << str << endl;
		return out;
	}
	//main battle result check function
	void ChangeStateFromInput(const string& userInput, queue<int>& computerInput)
	{
		int index;
		if (userInput == "g") { index = 0; m_userInputStr = "g"; }
		if (userInput == "c") { index = 1; m_userInputStr = "c"; }
		if (userInput == "p") { index = 2; m_userInputStr = "p"; }
		m_gameState = stateTable[index][computerInput.back()];
		if (computerInput.back() == 0) { m_computerInputStr = "g"; }
		if (computerInput.back() == 1) { m_computerInputStr = "c"; }
		if (computerInput.back() == 2) { m_computerInputStr = "p"; }
		computerInput.pop();
	}
	//show the players infomation at recent
	ostream& ShowInfoText(ostream& out) const
	{
		out << "you hp = " << m_user->GetHP() << ", enemy hp = " << m_computer->GetHP() << endl;
		return out;
	}
	//output the janken text
	ostream& ShowInputText(ostream& out) const
	{
		out << "janken ? ";
		return out;

	}
private:
	unique_ptr<Player> m_user;
	unique_ptr<Player> m_computer;
	StateFlag m_gameState;
	string m_computerInputStr;	//record the computer player input command
	string m_userInputStr;		//record the user player input command
	static const StateFlag stateTable[3][3];

};

//stateTable,for checking the battle result
const MushiKing::StateFlag MushiKing::stateTable[3][3] =
{ {UDeuce, UWIn, ULose},{ULose, UDeuce, UWIn},{UWIn, ULose, UDeuce} };

int main(int argc, char* argv[]) {

	string str = argv[1];
	const auto seed = atoi(str.c_str());

	//random engine
	mt19937 mt(seed);

	uniform_int_distribution<> range_rnd{ 0 , 2 };

	//computer input command queue
	queue<int> qComputerCommandQueue;

	//if the game is over
	bool done = false;

	//initialize game
	auto mushikingGame = make_unique<MushiKing>();
	mushikingGame->Initialize(50, 20, 50, 10);

	//game loop
	while (!done)
	{
		//process the computerPlayer input command queue
		if (qComputerCommandQueue.empty())
		{
			qComputerCommandQueue.push(range_rnd(mt));
		}
		//process the Input of user and computor players
		mushikingGame->Input(cin, qComputerCommandQueue);
		//update game object
		done = mushikingGame->Update();
		//output process result
		mushikingGame->ShowText(cout);
	}

	//release the game resourse
	mushikingGame->Shutdown();
	mushikingGame.reset();
	return 0;
}